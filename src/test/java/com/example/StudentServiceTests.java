package com.example;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.example.model.StudentModel;
import com.example.service.StudentService;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class StudentServiceTests {
	@Autowired
	StudentService service;
	
	@Test
	public void testSelectAllStudents()
	{
	 List<StudentModel> students = service.selectAllStudents();
	 
	 Assert.assertNotNull("Gagal = student menghasilkan null", students);
	 
	 Assert.assertEquals("Gagal - size students tidak sesuai", 2, students.size());
	}
	
	@Test
	public void testSelectStudent()
	{
	 StudentModel student = service.selectStudent("123");
	 
	 Assert.assertNotNull("Gagal = student menghasilkan null", student);
	 
	 Assert.assertEquals("Gagal - nama students tidak sesuai", "student 1", student.getName());
	 
	 Assert.assertEquals("Gagal - GPA student tidak sesuai", 1.92, student.getGpa(), 0.0);
	}
	
	@Test
	public void testCreateStudent() {
		
		StudentModel student = new StudentModel("126", "Budi", 3.44, null);
		
		// Cek apakah student sudah ada
		Assert.assertNull("Mahasiswa sudah ada", service.selectStudent(student.getNpm()));
		
		// Masukkan ke service
		service.addStudent(student);
		
		// Cek apakah student berhasil dimasukkan
		Assert.assertNotNull("Mahasiswa gagal dimasukkan", service.selectStudent(student.getNpm()));
	}
	
	
	@Test
	public void testUpdateStudent() {
		//Ambil satu student
		StudentModel student = service.selectStudent("123");
		
		//Ubah GPA student
		student.setGpa(1.00);
		
		//Update student di service
		service.updateStudent(student);
		
		//Ambil Kembali student menggunakan service
		student = service.selectStudent("123");
		
		// Cek apakah gpa student berhasil diubah
		Assert.assertEquals("Gagal - GPA student tidak sesuai dengan yang sudah diupdate", 1.00, student.getGpa(), 0.0);
	}
	
	@Test
	public void testDeleteStudent() {
		//Ambil satu student
		StudentModel student = service.selectStudent("123");
		
		//Cek apakah student bernilai null
		Assert.assertNotNull("Gagal = student menghasilkan null", student);
		
		//Delete student menggunakan service
		service.deleteStudent("123");
		
		//Ambil kembali student yg sama (yang sudah dihapus)
		student = service.selectStudent("123");
		
		//Cek apakah student bernilai null
		//Jika bernilai null, maka unit testing untuk hapus berhasil
		Assert.assertNotNull("Gagal = student menghasilkan null", student);
	}
}